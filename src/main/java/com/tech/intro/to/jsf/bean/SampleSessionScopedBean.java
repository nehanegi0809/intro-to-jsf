/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.intro.to.jsf.bean;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author neha
 */
@Named("sampleSessionBean")
@SessionScoped
public class SampleSessionScopedBean implements Serializable {
    
}
