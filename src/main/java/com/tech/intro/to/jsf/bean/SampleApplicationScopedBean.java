/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.intro.to.jsf.bean;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author neha
 */
@Named("sampleAppScopedBean")
@ApplicationScoped
public class SampleApplicationScopedBean {
    
}
